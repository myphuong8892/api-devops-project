variable "prefix" {
  default = "app-api-devops"
}
variable "project" {
  default = "app-api-devops"
}
variable "contact" {
  default = "myphuong8892@gmail.com"
}
variable "db_username" {
  description = "Username for the RDS Postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}
variable "bastion_key_name" {
  default = "django-api-bastion"
}
variable "ecr_image_api" {
  description = "ECR Image for API"
  default     = "431610741011.dkr.ecr.us-east-1.amazonaws.com/app-api-devops-project:latest"
}

variable "ecr_image_proxy" {
  description = "ECR Image for API"
  default     = "431610741011.dkr.ecr.us-east-1.amazonaws.com/api-devops-proxy:latest"
}
//will set it in gitlab ci 
variable "django_secret_key" {
  description = "Secret key for Django app"
}

